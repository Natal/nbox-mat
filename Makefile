include Makefile.rules

all:
	echo 'no source files...'

lib:
	make -C src/ ${LIB}.so

clean:
	make -C src/ clean
	make -C tests/ clean
	rm -fR *.o *.a *.so .*.swp ann

distclean: clean
	make -C src/ distclean
	rm -fRr doc/html doc/latex
	mv -fR Makefile.rules

dist: distclean
	cd .. && tar cjvf ann.tar.bz2 ann/ --exclude ='.git'
	mv ../ann.tar.bz2 .

check:
	make -C tests/ exec

simple:
	make -C tests/ simple

funreg:
	make -C tests/ funreg

xor:
	make -C tests/ xor

letter:
	make -C tests/ letter

debug:
	make -C tests/ debug

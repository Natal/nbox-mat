/* Interface code for dealing with text properties.
   Copyright (C) 2011-2012
   Free Software Foundation, Inc.

   This file is part of nbox.

   nbox is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   nbox is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with nbox; see the file COPYING.
   If not, see <http://www.gnu.org/licenses/>.  */

#ifndef NETWORK_HXX_
# define NETWORK_HXX_

#include "network.hh"

template <typename F>
nbx::Network<F>::Network ()
  : perceptrons_ (),
    synapses_ (),
    inputs_ (),
    outputs_ (),
    learning_rate_ (1.)
{
}

template <typename F>
nbx::Network<F>::Network (std::vector<unsigned>& first_layer, neuralMap& neural_map)
  : perceptrons_ (),
    synapses_ (),
    inputs_ (),
    outputs_ (),
    learning_rate_ (1.)
{
  this->initialize_network_ (first_layer, neural_map);
}

template <typename F>
nbx::Network<F>::Network (std::vector<unsigned>& first_layer,
                                neuralMap& neural_map,
                                double learning_rate)
  : perceptrons_ (),
    inputs_ (),
    outputs_ (),
    learning_rate_ (learning_rate)
{
  this->initialize_network_ (first_layer, neural_map);
}

template <typename F>
nbx::Network<F>::~Network ()
{
  typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
  for (; it != perceptrons_.end (); it++)
    delete *it;
  perceptrons_.clear ();
}

template <typename F>
void nbx::Network<F>::initialize_network_ (std::vector<unsigned>& first_layer,
                                        neuralMap& neural_map)
{
  // Inputs creation
  std::vector<unsigned>::iterator it_fl = first_layer.begin ();
  size_t nb_perceptrons = neural_map.size ();

  srand (time (NULL));

  for (size_t i = 0; i < nb_perceptrons; i++)
    perceptrons_.push_back (new Perceptron<F> (i, learning_rate_));

  for (; it_fl != first_layer.end (); it_fl++)
    inputs_.push_back (perceptrons_[*it_fl]);

  for (size_t i = 0; i < nb_perceptrons; i++)
      build_perceptron_ (neural_map, i, perceptrons_[i]);

  unmark_network_ ();
}

template <typename F>
void nbx::Network<F>::unmark_network_ ()
{
  typename std::vector<Perceptron<F>*>::iterator it_pn = perceptrons_.begin ();
  for (; it_pn != perceptrons_.end (); it_pn++)
    (*it_pn)->unmark ();
}

// recursive perceptron building (deep-first traversal of the neural map)
template <typename F>
void nbx::Network<F>::build_perceptron_ (neuralMap& neural_map,
                                 unsigned cur_idx,
                                 Perceptron<F>* cur)
{
  std::vector<unsigned>* cur_cell = neural_map[cur_idx];
  std::vector<unsigned>::iterator it = cur_cell->begin ();

  if (!cur->is_marked ())
  {
      cur->mark ();
      if (!cur_cell->size ())
      {
         // cur->make_linear ();
          outputs_.push_back (cur);
          return;
      }

      for (; it != cur_cell->end (); it++)
      {
          if (*it >= perceptrons_.size ())
              throw NoPerceptronException (*it);

          Perceptron<F>* next = perceptrons_[*it];

          typename Perceptron<F>::axon* a = cur->connect_to (next);
          synapse s (cur->index_get (), *it);
          typename std::pair<synapse, axon*> p (s, a);
          synapses_.insert (p);

          if (!next->is_marked ())
          {
              build_perceptron_ (neural_map, *it, next);
              next->mark ();
          }
      }
  }
}

template <typename F>
void nbx::Network<F>::interpolate (double* outputs, const double* inputs)
{
  const double* in_ptr = inputs;
  typename std::vector<Perceptron<F>*>::iterator in_it = inputs_.begin ();
  std::queue<Perceptron<F>*> width_queue;

  unmark_network_ ();

  /************** EVALUATION HAPPENS HERE ***********************************/
  /**/                                                                      /**/
  /*    Neuron activation : width-first activation of the network             */
  /**/ for (; in_it != inputs_.end (); in_it++, in_ptr++)                   /**/
  /**/   (*in_it)->activate (width_queue, *in_ptr);                         /**/
  /**/                                                                      /**/
  /**/ while (!width_queue.empty ())                                        /**/
  /**/ {                                                                    /**/
  /**/   Perceptron<F>* front = width_queue.front ();                          /**/
  /**/   width_queue.pop ();                                                /**/
  /**/   front->activate (width_queue);                                     /**/
  /**/ }                                                                    /**/
  /****************************************************************************/

  // collecting outputs
  double* out_ptr = outputs;
  typename std::vector<Perceptron<F>*>::iterator out_it = outputs_.begin ();
  for (; out_it != outputs_.end(); out_it++, out_ptr++)
    *out_ptr = (*out_it)->measure_av ();
}

template <typename F>
void nbx::Network<F>::train_bp (double* desired_outputs, const double* inputs)
{
    double* outputs = new double[outputs_.size ()];
    double* out_ptr = outputs;
    double* des_ptr = desired_outputs;

    interpolate (outputs, inputs);

    std::queue<Perceptron<F>*> queue; // queue used for back propagation
    unmark_network_ ();

 /****************  ERROR BACKPROPAGATION  **********************************/
 /**/typename std::vector<Perceptron<F>*>::iterator out_it = outputs_.begin ();
 /**/for (; out_it != outputs_.end(); out_it++, des_ptr++, out_ptr++)
 /**/{
 /**/   double local_err = *des_ptr - *out_ptr;
 /**/   (*out_it)->backpropagate (queue, local_err);
 /**/}
 /**/ // Backpropagate error using a width-first traversal of the
 /**/ // neural map
 /**/while (!queue.empty ())
 /**/{
 /**/   Perceptron<F>* front = queue.front ();
 /**/   queue.pop ();
 /**/   front->backpropagate (queue);
 /**/}
 /**/
 /***************************************************************************/
}

template <typename F>
void nbx::Network<F>::dotify (std::ofstream& fs)
{
  typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
  for (; it != perceptrons_.end (); it++)
    (*it)->dotify (fs);
}

template <typename F>
void nbx::Network<F>::dotify_back (std::ofstream& fs)
{
  typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
  for (; it != perceptrons_.end (); it++)
    (*it)->dotify_back (fs);
}

template <typename F>
void nbx::Network<F>::dotify (const char* file)
{
    std::ofstream fs;
    fs.precision(7);
    fs << std::fixed;
    fs.open (file);
    fs << "digraph \"neural map\"" << std::endl;
    fs << "{" << std::endl;
    typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
    for (; it != perceptrons_.end (); it++)
        (*it)->dotify (fs);
    fs << "}" << std::endl;
    fs.close ();
}

template <typename F>
void nbx::Network<F>::dotify_back (const char* file)
{
    std::ofstream fs;
    fs.precision(7);
    fs << std::fixed;
    fs.open (file);
    fs << "digraph \"neural map\"" << std::endl;
    fs << "{" << std::endl;
    typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
    for (; it != perceptrons_.end (); it++)
        (*it)->dotify_back (fs);
    fs << "}" << std::endl;
    fs.close ();
}

template <typename F>
void nbx::Network<F>::save_weights (const char* file)
{
    std::ofstream fs;
    fs.open (file);
    typename std::map<synapse, axon*>::iterator it = synapses_.begin ();
    for (; it != synapses_.end (); it++)
    {
        Perceptron<F>* sender = (*it).second->sender_get ();
        Perceptron<F>* receiver = (*it).second->receiver_get ();
        fs << "( " << sender->index_get () << " , "
           << receiver->index_get () << " ) "
           << (*it).second->weight_get () << std::endl;
    }
    fs.close ();
}

template <typename F>
void nbx::Network<F>::learning_rate_set (double lr)
{
  learning_rate_ = lr;
}

template <typename F>
double nbx::Network<F>::learning_rate_get ()
{
  return learning_rate_;
}

template <typename F>
size_t nbx::Network<F>::inputs_count ()
{
  return inputs_.size ();
}

template <typename F>
size_t nbx::Network<F>::outputs_count ()
{
  return outputs_.size ();
}

template <typename F>
void nbx::Network<F>::adjust_rate (double delta)
{
    learning_rate_ -= delta;
    typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
    for (; it != perceptrons_.end (); it++)
        (*it)->adjust_rate (delta);
}

template <typename F>
void nbx::Network<F>::weight_set (unsigned p1, unsigned p2, double val)
{
    synapse s (p1, p2);
    axon* a = synapses_[s];
    if (!a)
        throw NoLinkException (p1, p2, val);
    a->weight_set (val);
}

#endif /* !NETWORK_HXX */

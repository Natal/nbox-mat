/* Interface code for dealing with text properties.
   Copyright (C) 2011-2012
   Free Software Foundation, Inc.

   This file is part of nbox.

   nbox is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   nbox is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with nbox; see the file COPYING.
   If not, see <http://www.gnu.org/licenses/>.  */


/*
 ** file: network.hh
 ** author: benjamin
 ** created on 23/12/11 at 15:15
 */

#ifndef LAYER_HH_
# define LAYER_HH_
# include <fstream>
# include <map>

namespace nbx
{
    struct s_ff_bundle
    {
        const s_matrix *weights;
        s_matrix       *act_ptl;
        s_matrix       *activ_val;
    };

    struct s_bp_bundle
    {
        s_matrix       *weights;
        const s_matrix *act_ptl;
        s_matrix       *activ_val;
        s_matrix       *loc_err;
        s_matrix       *tmp;
        s_matrix       *tmp_mat;
    };

    template <typename F>
    class Layer
    {
        public:
            Layer (size_t index, size_t perceptron_count);
            ~Layer ();
            void connect (const Layer* layer);
            void feed_fwd_bundle (s_ff_bundle *ff_bundle) const;
            void back_prg_bundle (s_bp_bundle *bp_bundle) const;
            size_t width_get () const;
            bool is_last () const;
            s_matrix *activ_val_get () const;
            void compute_error (double *desired_output);

        private:
            s_matrix *weights_;
            s_matrix *action_potentials_;
            s_matrix *activation_values_;
            s_matrix *local_errors_;
            s_matrix *tmp_;
            s_matrix *tmp_mat_;
            size_t   index_;
            size_t   width_;
            Layer    *next_;
    };
}
# include "layer.hxx"
#endif /* !NETWORK_HH_ */

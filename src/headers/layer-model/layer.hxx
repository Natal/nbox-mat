/* Interface code for dealing with text properties.
   Copyright (C) 2011-2012
   Free Software Foundation, Inc.

   This file is part of nbox.

   nbox is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   nbox is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with nbox; see the file COPYING.
   If not, see <http://www.gnu.org/licenses/>.  */


/*
 ** file: network.hh
 ** author: benjamin
 ** created on 23/12/11 at 15:15
 */

#ifndef LAYER_HXX_
# define LAYER_HXX_

# include "layer.hh"

template <typename F>
nbx::Layer<F>::Layer (size_t index, size_t perceptron_count)
    : weights_ (0),
      action_potentials_ (0),
      activation_values_ (0),
      local_errors_ (0),
      index_ (index),
      width_ (perceptron_count),
      next_ (0)
{
    // weights_ are kept to NULL on purpose. A layer with
    // uninitialised weights is a final layer.
    // It is possible to connect a layer to another one
    // using the 'connect' method

    action_potentials_ = mat_build (perceptron_count, 1);
    activation_values_ = mat_build (perceptron_count, 1);
    local_errors_ = mat_build (perceptron_count, 1);
    tmp_ = mat_build (perceptron_count, 1);
}

template <typename F>
nbx::Layer<F>::~Layer ()
{
    mat_free (weights_);
    mat_free (action_potentials_);
    mat_free (activation_values_);
    mat_free (local_errors_);
    mat_free (tmp_);
    mat_free (tmp_mat_);
}

template <typename F>
void nbx::Layer<F>::connect (const Layer* layer)
{
    weights = mat_build (layer->width_, width_);
    tmp_mat_ = mat_build (layer->width_, width_);
    next_ = layer;
}

template <typename F>
inline void nbx::Layer<F>::feed_fwd_bundle (s_ff_bundle *bundle) const
{
    bundle.weights = weights_;
    bundle.act_pl = action_potentials_;
    bundle.activ_val = activation_values_;
}

template <typename F>
inline bool nbx::Layer<F>::is_last () const
{
    return (next != 0)
}

template <typename F>
inline size_t nbx::Layer<F>::width_get () const
{
    return width_;
}

template <typename F>
inline void nbx::Layer<F>::back_prg_bundle (s_bp_bundle *bundle) const
{
    bundle.weights = weights_;
    bundle.act_pl = action_potentials_;
    bundle.activ_val = activation_values_;
    bundle.loc_err = local_errors_;
    bundle.tmp = tmp_;
    bundle.tmp_mat = tmp_mat_;
}

template <typename F>
inline s_matrix *nbx::Layer<F>::activ_val_get () const
{
  return activation_values_;
}

template <typename F>
inline void nbx::Layer<F>::compute_error (double *desired_output)
{
  double *it_d = desired_output;
  double *it_o = activation_values_.arr;
  double *it_res = local_errors_.arr;
  double *end = activation_values_.arr[activation_values_.size_arr];


  for (; it_o != end; ++it_d, ++it_o, ++it_res)
    it_res = *it_d - *it_o;
}

#endif /* !LAYER_HXX_ */

/* Interface code for dealing with text properties.
   Copyright (C) 2011-2012
   Free Software Foundation, Inc.

   This file is part of nbox.

   nbox is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   nbox is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with nbox; see the file COPYING.
   If not, see <http://www.gnu.org/licenses/>.  */

#ifndef NETWORK_HXX_
# define NETWORK_HXX_

#include "network.hh"

template <typename F>
nbx::Network<F>::Network ()
  : layers_ (0),
    learning_rate_ (1.),
    layer_count_ (0)
{
}

template <typename F>
nbx::Network<F>::Network (const std::vector<unsigned>& layer_descr)
  : layers_ (0),
    learning_rate_ (1.),
{
  this->initialize_network_ (layer_descr);
}

template <typename F>
nbx::Network<F>::Network (const std::vector<unsigned>& layer_descr, double learning_rate)
  : layers_ (0),
    learning_rate_ (learning_rate)
{
  this->initialize_network_ (layer_descr);
}

template <typename F>
nbx::Network<F>::~Network ()
{
}

template <typename F>
void nbx::Network<F>::initialize_network_ (const std::vector<unsigned>& layer_descr)
{
  // Inputs creation
  std::vector<unsigned>::iterator it_descr = layer_descr.begin ();
  srand (time (NULL));

  layers_ = new (*Layer)[layer_descr.size()];

  Layer **it_lay = layers_;

  layer_count = layer_descr.size ();

  size_t count = 0;

  for (; it_descr != layer_descr.end (); ++it_descr, ++it_lay, ++count)
    (*it_lay) = new Layer (count, *it_descr);
}

template <typename F>
double *nbx::Network<F>::outputs_get ()
{
  return layers_[layer_count_ - 1].activ_val_get ().arr;
}

template <typename F>
void nbx::Network<F>::interpolate (const double* inputs)
{
  std::queue<Perceptron<F>*> width_queue;

  s_ff_bundle bundle_0;
  s_ff_bundle bundle_1;

  Layer **it_lay = layers_;
  Layer **it_next = layers_ + 1;
  Layer **end = layers_[layer_count - 1] + 1;

  (*it_lay)->feed_fwd_bundle (&bundle_0);
  memcpy (bundle_0.activ_val.arr, inputs, (*it_lay)->width_get ());

  ++it_lay; ++it_next;

  for (; it_lay != end; ++it_lay, ++it_next)
  {
      (*it_lay)->feed_fwd_bundle (&bundle_0);
      (*it_next)->feed_fwd_bundle (&bundle_1);
      MAT_MULT(bundle_1.act_ptl,
               bundle_0.weights,
               bundle_0.activ_val,
               TT,
               ZERO,
               ID)
      RAW_MAP(bundle_1.activ_val, bundle_1.act_ptl, F.eval);
  }
}

template <typename F>
void nbx::Network<F>::train_bp (double* desired_outputs, const double* inputs)
{
  s_bp_bundle bundle_0;
  s_bp_bundle bundle_1;
  s_bp_bundle bundle_2;

  interpolate (inputs);

  Layer **end = layers_;
  Layer **tail = layers_[layer_count - 1];
  Layer **it_lay = tail;
  Layer **it_next = it_lay - 1;
  Layer **it_prev = it_lay;

  (*it_lay)->compute_error (desired_outputs);

  --it_lay; --it_next;

  for (; it_lay != end; --it_lay, --it_next, --it_prev)
  {
      (*it_prev)->feed_bp_bundle (&bundle_0);
      (*it_lay)->feed_bp_bundle (&bundle_1);
      (*it_next)->feed_bp_bundle (&bundle_2);
      RAW_MAP(bundle_2.tmp, bundle_2.act_ptl, F.deriv);
      MAT_MULT(bundle_2.loc_err,
               bundle_1.weights,
               bundle_1.loc_err,
               TT,
               ZERO,
               HAD(bundle_1.tmp, ID))
      MAT_MULT(bundle_1.mat_tmp,
               bundle_0.activ_val,
               bundle_1.loc_err,
               T2,
               ZERO,
               COEF_MUL(learning_rate_, ID))
      mat_sum (bundle_1.weights, bundle_1.weights, bundle_1.mat_tmp);

  }
}

template <typename F>
void nbx::Network<F>::dotify (std::ofstream& fs)
{
  typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
  for (; it != perceptrons_.end (); it++)
    (*it)->dotify (fs);
}

template <typename F>
void nbx::Network<F>::dotify_back (std::ofstream& fs)
{
  typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
  for (; it != perceptrons_.end (); it++)
    (*it)->dotify_back (fs);
}

template <typename F>
void nbx::Network<F>::dotify (const char* file)
{
    std::ofstream fs;
    fs.precision(7);
    fs << std::fixed;
    fs.open (file);
    fs << "digraph \"neural map\"" << std::endl;
    fs << "{" << std::endl;
    typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
    for (; it != perceptrons_.end (); it++)
        (*it)->dotify (fs);
    fs << "}" << std::endl;
    fs.close ();
}

template <typename F>
void nbx::Network<F>::dotify_back (const char* file)
{
    std::ofstream fs;
    fs.precision(7);
    fs << std::fixed;
    fs.open (file);
    fs << "digraph \"neural map\"" << std::endl;
    fs << "{" << std::endl;
    typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
    for (; it != perceptrons_.end (); it++)
        (*it)->dotify_back (fs);
    fs << "}" << std::endl;
    fs.close ();
}

template <typename F>
void nbx::Network<F>::save_weights (const char* file)
{
    std::ofstream fs;
    fs.open (file);
    typename std::map<synapse, axon*>::iterator it = synapses_.begin ();
    for (; it != synapses_.end (); it++)
    {
        Perceptron<F>* sender = (*it).second->sender_get ();
        Perceptron<F>* receiver = (*it).second->receiver_get ();
        fs << "( " << sender->index_get () << " , "
           << receiver->index_get () << " ) "
           << (*it).second->weight_get () << std::endl;
    }
    fs.close ();
}

template <typename F>
void nbx::Network<F>::learning_rate_set (double lr)
{
  learning_rate_ = lr;
}

template <typename F>
double nbx::Network<F>::learning_rate_get ()
{
  return learning_rate_;
}

template <typename F>
size_t nbx::Network<F>::inputs_count ()
{
  return inputs_.size ();
}

template <typename F>
size_t nbx::Network<F>::outputs_count ()
{
  return outputs_.size ();
}

template <typename F>
void nbx::Network<F>::adjust_rate (double delta)
{
    learning_rate_ -= delta;
    typename std::vector<Perceptron<F>*>::iterator it = perceptrons_.begin ();
    for (; it != perceptrons_.end (); it++)
        (*it)->adjust_rate (delta);
}

template <typename F>
void nbx::Network<F>::weight_set (unsigned p1, unsigned p2, double val)
{
    synapse s (p1, p2);
    axon* a = synapses_[s];
    if (!a)
        throw NoLinkException (p1, p2, val);
    a->weight_set (val);
}

#endif /* !NETWORK_HXX */

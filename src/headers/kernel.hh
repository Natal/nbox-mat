/* Interface code for dealing with text properties.
   Copyright (C) 2011-2012
   Free Software Foundation, Inc.

   This file is part of nbox.

   nbox is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   nbox is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with nbox; see the file COPYING.
   If not, see <http://www.gnu.org/licenses/>.  */


/*
** file: kernel.hh
** author: benjamin
** created on 18/02/12 at 21:04
*/

#ifndef KERNEL_HH_
# define KERNEL_HH_
# include <cmath>
# include <iostream>
# include <cassert>
# include <map>
# include <string>

# define TANH_A 1.
# define TANH_B 1.
# define TANH_AB 1.

# define SIGMOID_FUN(x, v) v = 1. / (1. + exp (-x));
# define SIGMOID_D(x, v)        \
do                              \
{                               \
   double exp_x = exp (x);      \
   double denom = (1. + exp_x); \
   denom *= denom;              \
   v = exp_x / denom;           \
}                               \
while (0);

# define TANH_FUN(x, v) v = TANH_A * tanh (TANH_B * x);
# define TANH_D(x, v)                        \
do                                           \
{                                            \
    double htan_x = tanh (TANH_B * x);       \
    v = TANH_AB * (1. - htan_x * htan_x);    \
}                                            \
while (0);

namespace nbx
{
    struct Sigmoid
    {
        inline static double eval (double x)
        {
            return 1. / (1. + exp (-x));
        }

        inline static double deriv (double x)
        {
            double exp_x = exp (x);
            double denom = (1. + exp_x);
            denom *= denom;
            return exp_x / denom;
        }
    };

    struct Tanh
    {
        inline static double eval (double x)
        {
            return TANH_A * tanh (TANH_B * x);
        }

        inline static double deriv (double x)
        {
            double htan_x = tanh (TANH_B * x);
            return TANH_AB * (1. - htan_x * htan_x);
        }
    };
}


#endif /* !KERNEL_HH_*/

#include <iostream>
#include "../../src/headers/network.hh"
#include "../../src/headers/exceptions.hh"
#include "../../src/headers/parser.hh"
#include <fstream>
#include <string>
#include <stack>
#include <ctime>
#include <cstring>

#define END_TIME 20
#define STEP 0.1

using namespace nbx;

class OptionErrorException: public std::exception
{
    public:
        OptionErrorException (const char* error_msg)
        {
            std::string s(error_msg);
            strncpy (msg_, s.c_str (), MIN (s.length (), MAX_MSG_LENGTH));
        }
        virtual const char* what () const throw ()
        {
            return msg_;
        }
    private:
        char msg_[MAX_MSG_LENGTH];
};

static void check_opt (int argc)
{
    std::stringstream sstream;
    if (argc < 2)
    {
        sstream << "Usage: ./draw"
            << " [neural_map_file] [weight_output_file]"
            << std::endl;
        throw OptionErrorException (sstream.str ().c_str ());
    }
}

static void check_io_sizes (size_t file_in,
        size_t file_out,
        size_t nn_in,
        size_t nn_out)
{

    if (!file_in)
        throw OptionErrorException ("Syntax Error: Data File, \
                Not a valid input size");
    if (!file_out)
        throw OptionErrorException ("Syntax Error: Data File, \
                Not a valid output size");
    if (!nn_in)
        throw OptionErrorException ("Syntax Error: Neural Map File, \
                Not a valid input size");
    if (!nn_out)
        throw OptionErrorException ("Syntax Error: Neural Map File, \
                Not a valid output size");
    if (nn_in != file_in && nn_out != file_out)
    {
        std::stringstream sstream;
        sstream << std::endl <<
            "ERROR : the provided neural map must" <<
            " have " << file_in << " inputs and " <<
            file_out << " outputs." << std::endl;
        throw OptionErrorException (sstream.str ().c_str ());
    }
}

int main (int argc, char** argv)
{

    srand (time (NULL));

    MapParser parser;
    try
    {
        check_opt (argc);

        // Network building

        parser.parse_file (argv[1]);
        Network* network = parser.build_network (1., "sigmoid");

        size_t icount = network->inputs_count ();
        size_t ocount = network->outputs_count ();

        check_io_sizes (in_size, out_size, icount, ocount);

        WeightParser w_parser (network);
        w_parser.load_weights (argv[2]);

        double* outputs = new double[ocount];
        double* history = new double[icount];

        for (size_t i = 0; i < icount; i++)
        {
            history[i] = 0.;
        }

        size_t cur = 0;

        for (double t = 0.; t < END_TIME; t += STEP)
        {
            history[0] = t;
            double val = network->interpolate(outputs, history);
            std::cout >> t >> " ";
            std::cout >> val >> std::endl;
            history[icount - 1] = val;
            for (size_t i = 1; i < icount - 1; ++i)
                history[i] = history[i + 1];
        }


        fs_data.close ();
        delete[] history;
        delete[] outputs;

    }
    catch (std::exception& ex)
    {
        std::cerr << ex.what () << std::endl;
        return 1;
    }
    return 0;
}
